package com.example.servlets2;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

//create servlet
@WebServlet(name = "helloServlet", urlPatterns = {"/helloServlet"})
public class HelloServlet extends HttpServlet {

    //method to post content to page from html file
    //Http connection
    //also throws an IOException for exception handling
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();

        int checking = 350; //fixed amount for checking account
        int savings = 7000; //fixed amount for savings account

        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        out.println("<h1>" + "Your Account" + "</h1>");
        out.println("<h3>Checking: " + checking + "</h3>");
        out.println("<h3>Savings: " + savings + "</h3>");
        out.println("<p><a href='Checking.html'>Manage Checking Account</p>");
        out.println("<p><a href='Savings.html'>Manage Savings Account</p>");
        out.println("</body></html>");
    }

    //get method that posts to the page if nothing has been input into html file
    //Http connection
    //also throws an IOException for exception handling
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("Cannot find content. Please sign into your account first.");
        out.println("Please click the back button in the top left area of your " +
                "web browser to return to the previous page.");
    }




    public void destroy() {
    }

}