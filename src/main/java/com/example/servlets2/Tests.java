package com.example.servlets2;

//JUnit tests

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.io.IOException;

public class Tests {

    HelloServlet helloServlet = new HelloServlet();

    @Test
    @DisplayName("AssertException")
    void test3(){
        System.out.println("--Testing for AssertException--");
        Assertions.assertThrows(IOException.class, (Executable) helloServlet);
    }

    @Test
    @DisplayName("AssertTrue")
    void test1(){
            System.out.println("--Testing using AssertTrue assertion--");
            boolean trueValue = true;
            boolean falseValue = false;
            Assertions.assertTrue(trueValue, "Test if true");
    }

    @Test
    @DisplayName("AssertFalse")
    void test2(){
        System.out.println("--Testing for AssertFalse assertion--");
        boolean trueValue = true;
        boolean falseValue = false;
        Assertions.assertFalse(falseValue, "Test if false");
    }

}
